<?php
function loadFile($className) {
    include_once str_replace('\\', '/', $className) . '.php';
}

spl_autoload_register('loadFile');


use bes\DateTime;
use bes\forms\Triangle as SaloperieATroisCotes;

$triangle = new SaloperieATroisCotes();
$triangle->sayHello();


$globalDateTime = new \DateTime();
echo $globalDateTime->format('c');

$localDAteTime = new DateTime();
echo $localDAteTime->getMyDate();


