<?php
include_once "Player.php";
include_once "Display.php";

$display = new Display();

$player1 = new Player();
$player1->name = "Romain";
$player1->health = 100;

$player2 = new Player();
$player2->name = "Louis";
$player2->health = 100;

$player1->hit($player2);
$player1->hit($player2);
$player1->hit($player2);
$player1->hit($player2);
$player2->hit($player1);
$player2->hit($player1);
$player2->hit($player1);

$display->playerStatus($player1);
$display->playerStatus($player2);
