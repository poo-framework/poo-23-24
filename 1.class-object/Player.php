<?php

class Player
{
    public string $name;
    public int $health;

    // : void est le type de la valeur de retour de la fonction (ici void car pas de return)
    // On met le type d'argument devant son nom (ici Player)
    public function hit(Player $victim): void {
        $victim->health -= 10; // Equivalent de $victim->health = $victim->health - 10;
    }

}
