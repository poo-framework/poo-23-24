<?php
include_once 'SvgDrawable.php';

abstract class FlatSurface implements SvgDrawable
{
    protected float $surface;
    protected int $perimeter;

    public function getSurface(): float
    {
        return $this->surface;
    }

    public function getPerimeter(): int
    {
        return $this->perimeter;
    }

    abstract public static function getSideNumber(): int;

    public function debug() {
        echo 'Je suis un '.get_class($this). " d'une surface de ". $this->getSurface(). " et d'un périmètre de " . $this->perimeter;
        echo '<br>';
    }


}
