<?php

class Rectangle extends FlatSurface
{
    protected int $width;
    protected int $height;

    /**
     * @param int $width
     * @param int $height
     */
    public function __construct(int $width, int $height)
    {
        $this->width = $width;
        $this->height = $height;

        $this->surface = $width * $height;
        $this->perimeter = $width * 2 + $height * 2;
    }


    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }


    public static function getSideNumber(): int
    {
        return 4;
    }

    public function getSvg(): string
    {
        return '<rect width="'.$this->width.'" height="'.$this->height.'" style="fill:rgb(0,0,255);stroke-width:3;stroke:rgb(0,0,0)" />';
    }
}
