<?php

class SvgCanvas
{

    public function openCanvas(): void {
        echo '<svg viewBox="0 0 1000 1000" xmlns="http://www.w3.org/2000/svg">';
    }

    public function drawForm(SvgDrawable $drawable) {
        echo $drawable->getSvg();
    }

    public function closeCanvas(): void {
        echo '</svg>';
    }

}
