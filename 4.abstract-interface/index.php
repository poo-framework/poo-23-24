<?php
include_once "FlatSurface.php";
include_once "Rectangle.php";
include_once "Square.php";
include_once "Circle.php";
include_once "Triangle.php";
include_once "SvgCanvas.php";

$rect = new Rectangle(200, 12);
$square = new Square(41);
$circle = new Circle(41);

$rect->debug();
$square->debug();
$circle->debug();


echo "Les cercles ont " . Circle::getSideNumber(). " côtés";
echo "Les carrés ont " . Square::getSideNumber(). " côtés";
echo "Les rectangles ont " . Rectangle::getSideNumber(). " côtés";

$triangle = Triangle::factory(100, 200);
$triangle->setBackgroundColor("#9933FF");

$canvas = new SvgCanvas();
$canvas->openCanvas();
$canvas->drawForm($triangle);
$canvas->drawForm($rect);
$canvas->drawForm($square);
$canvas->drawForm($circle);
$canvas->closeCanvas();


