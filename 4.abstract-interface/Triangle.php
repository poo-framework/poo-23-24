<?php

class Triangle extends FlatSurface
{

    private int $base;
    private int $height;
    private int $heightPlace;

    private string $backgroundColor;
    private string $borderColor;
    private int $rotation;

    public function getBase(): int
    {
        return $this->base;
    }

    public function setBase(int $base): void
    {
        $this->base = $base;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function setHeight(int $height): void
    {
        $this->height = $height;
    }

    public function getHeightPlace(): int
    {
        return $this->heightPlace;
    }

    public function setHeightPlace(int $heightPlace): void
    {
        $this->heightPlace = $heightPlace;
    }

    public function getBackgroundColor(): string
    {
        return $this->backgroundColor;
    }

    public function setBackgroundColor(string $backgroundColor): void
    {
        $this->backgroundColor = $backgroundColor;
    }

    public function getBorderColor(): string
    {
        return $this->borderColor;
    }

    public function setBorderColor(string $borderColor): void
    {
        $this->borderColor = $borderColor;
    }

    public function getRotation(): int
    {
        return $this->rotation;
    }

    public function setRotation(int $rotation): void
    {
        $this->rotation = $rotation;
    }



    public static function getSideNumber(): int
    {
        return 3;
    }

    public function getSvg(): string
    {
        return '<polygon points="0,0 '.$this->base.',0 '.$this->base.','.$this->height.'" style="fill:'.$this->backgroundColor.';stroke:'.$this->borderColor.';stroke-width:2" class="triangle" />';
    }

    public static function factory(int $base, int $height): Triangle {
        $triangle = new self;
        $triangle->setBase($base);
        $triangle->setHeight($height);
        $triangle->setRotation(0);
        $triangle->setBackgroundColor('#00FF00');
        $triangle->setBorderColor('#000000');
        $triangle->setHeightPlace(0);

        return $triangle;
    }

}
