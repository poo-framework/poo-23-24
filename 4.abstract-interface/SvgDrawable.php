<?php

interface SvgDrawable
{
    public function getSvg(): string;
}
