<?php

class Circle extends FlatSurface
{
    private int $radius;

    /**
     * @param int $radius
     */
    public function __construct(int $radius)
    {
        $this->radius = $radius;

        $this->surface = pi() * ($this->radius * $this->radius);
        $this->perimeter = 2 * pi() * $this->radius;
    }

    public function getRadius(): int
    {
        return $this->radius;
    }

    public static function getSideNumber(): int
    {
        return 0;
    }

    public function getSvg(): string
    {
        return '<circle cx="50" cy="50" r="' . $this->radius . '" />';
    }
}
