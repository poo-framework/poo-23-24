<?php

class Triangle
{
    public const SURFACE_FORMULA = "base * height / 2";
    private int $base;
    private int $height;
    private int $surface;

    public function __construct(int $base, int $height)
    {
        $this->setBase($base);
        $this->setHeight($height);
    }

    public function getSurface(): int
    {
        return $this->surface;
    }

    public function getBase(): int
    {
        return $this->base;
    }

    public function setBase(int $base): void
    {
        if ($base < 1) {
            throw new Exception("La base d'un triangle doit être > 0, " . $base . " fourni");
        }
        $this->base = $base;
        if (isset($this->height) && isset($this->base)) {
            $this->surface = self::processSurface($this->base, $this->height);
        }

    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function setHeight(int $height): void
    {
        if ($height < 1) {
            throw new Exception("La hauteur d'un triangle doit être > 0, " . $height . " fourni");
        }
        $this->height = $height;
        if ($this->height && $this->base) {
            $this->surface = self::processSurface($this->base, $this->height);
        }
    }


    public function __toString(): string
    {
        return "Je suis un triangle avec une base de ". $this->base . " et une hauteur de " . $this->height .
            " pour une surface totale de ". $this->surface . "<br>";
    }

    public static function processSurface(int $base, int $height): int {
        return ($base * $height) / 2;
    }

}
