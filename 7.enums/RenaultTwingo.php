<?php

class RenaultTwingo
{
    private bool $hasAC;
    private TwingoColors $color;

    /**
     * @param bool $hasAC
     * @param TwingoColors $color
     */
    public function __construct(bool $hasAC, TwingoColors $color)
    {
        $this->hasAC = $hasAC;
        $this->color = $color;
    }


    public function isHasAC(): bool
    {
        return $this->hasAC;
    }

    public function setHasAC(bool $hasAC): void
    {
        $this->hasAC = $hasAC;
    }

    public function getColor(): TwingoColors
    {
        return $this->color;
    }

    public function setColor(TwingoColors $color): void
    {
        $this->color = $color;
    }




}
