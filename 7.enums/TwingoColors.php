<?php

enum TwingoColors: string
{
    case JAUNE_INDIEN = "JAUNE_INDIEN";
    case ROUGE_CORAIL = "#CC4367";
    case BLEU_OUTREMER = "#4367CC";
    case VERT_CORIANDRE = "VERT_CORIANDRE";
    case NOIR_INTENSE = "NOIR_INTENSE";
}
