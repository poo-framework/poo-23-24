<?php

use game\Dwarf;
use game\Elf;
use game\Wizard;

function loadFile($className) {
    include_once str_replace('\\', '/', $className) . '.php';
}

spl_autoload_register('loadFile');


$wiz = new Wizard('Gandalf', 100, 100);
$dwarf = new Dwarf('Gimli', 100, 100);
$elf = new Elf('Legolas', 100, 100);


$wiz->hit($dwarf);
$wiz->hit($elf);
$wiz->hit($elf);
$wiz->hit($elf);


$dwarf->hit($wiz);
$dwarf->hit($wiz);
$dwarf->hit($wiz);
$dwarf->hit($elf);
