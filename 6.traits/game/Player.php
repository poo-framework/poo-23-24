<?php

namespace game;

abstract class Player
{

    private string $name;
    private int $health;
    private int $strength;

    /**
     * @param string $name
     * @param int $health
     * @param int $strength
     */
    public function __construct(string $name, int $health, int $strength)
    {
        $this->name = $name;
        $this->health = $health;
        $this->strength = $strength;
    }


    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function setHealth(int $health): void
    {
        $this->health = $health;
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function setStrength(int $strength): void
    {
        $this->strength = $strength;
    }

    public abstract function takeHit(Player $hitter): void;
    public abstract function hit(Player $victim): void;

}
