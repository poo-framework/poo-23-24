<?php

namespace game;

class Dwarf extends Player
{

    public function takeHit(Player $hitter): void
    {
        $this->setHealth($this->getHealth() - $hitter->getStrength() / 2);
    }

    public function hit(Player $victim): void
    {
        $victim->takeHit($this);
    }
}
