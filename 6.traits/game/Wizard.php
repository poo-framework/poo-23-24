<?php

namespace game;

class Wizard extends Player
{
    use Cryer;

    public function takeHit(Player $hitter): void
    {
        if (rand(0,1) % 2 === 0) {
            $this->setHealth($this->getHealth() - $hitter->getStrength());
            $this->cry();
        }
    }

    public function hit(Player $victim): void
    {
        $victim->takeHit($this);
    }
}
