<?php

namespace game;

trait Cryer
{
    public function cry(): void {
        echo get_class($this). ': BouOuouOuouOuh snif <br>';
    }

}
