<?php

namespace game;

class Elf extends Player
{
    use Cryer;

    public function takeHit(Player $hitter): void
    {
        $this->setHealth($this->getHealth() - $hitter->getStrength());
        $this->cry();
    }

    public function hit(Player $victim): void
    {
        $victim->takeHit($this);
    }
}
