<?php

class ImmutableTriangle
{
    public const SURFACE_FORMULA = "base * height / 2";

    protected int $base;
    protected int $height;
    protected int $surface;

    /**
     * @param $base
     * @param $height
     */
    public function __construct($base, $height)
    {
        if ($base < 1) {
            throw new Exception("La base d'un triangle doit être > 0, " . $base . " fourni");
        }
        if ($height < 1) {
            throw new Exception("La hauteur d'un triangle doit être > 0, " . $height . " fourni");
        }
        $this->base = $base;
        $this->height = $height;
        $this->surface = self::processSurface($this->base, $this->height);
    }


    public function getSurface(): int
    {
        return $this->surface;
    }

    public function getBase(): int
    {
        return $this->base;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function __toString(): string
    {
        return "Je suis un triangle avec une base de ". $this->base . " et une hauteur de " . $this->height .
            " pour une surface totale de ". $this->surface . "<br>";
    }

    public static function processSurface(int $base, int $height): int {
        return ($base * $height) / 2;
    }

}
