<?php

class Sayen extends Player
{
    public function hit(Player $victim): void
    {
        $victim->health -= $this->strength;
        $this->strength += 20;
    }
}
