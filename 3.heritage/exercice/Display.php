<?php

class Display
{
    public function playerStatus(Player $player): void {
        echo "Player " . $player->getName(). " (force: " . $player->getStrength(). "): " . $player->getHealth() . " health </br>";
    }
}
