<?php

class Human extends Player
{
    public function hit(Player $victim): void
    {
        $victim->health -= $this->strength / 2;
    }
}
