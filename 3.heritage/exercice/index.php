<?php
include_once "Player.php";
include_once "Sayen.php";
include_once "Human.php";
include_once "Display.php";

$display = new Display();

$player1 = new Sayen("Romain", 10000, 1000);
$player2 = new Human("Louis", 10000, 100);

$player1->hit($player2);
$player1->hit($player2);
$player1->hit($player2);
$player1->hit($player2);
$player2->hit($player1);
$player2->hit($player1);
$player2->hit($player1);

$display->playerStatus($player1);
$display->playerStatus($player2);
