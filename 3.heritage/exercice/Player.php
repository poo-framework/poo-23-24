<?php

class Player
{
    protected string $name;
    protected int $health;
    protected int $strength;

    // : void est le type de la valeur de retour de la fonction (ici void car pas de return)
    // On met le type d'argument devant son nom (ici Player)
    /**
     * @param string $name
     * @param int $health
     * @param int $strength
     */
    public function __construct(string $name, int $health, int $strength)
    {
        $this->name = $name;
        $this->health = $health;
        $this->strength = $strength;
    }

    public function hit(Player $victim): void {
        $victim->health -= 10; // Equivalent de $victim->health = $victim->health - 10;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function getStrength(): int
    {
        return $this->strength;
    }



}
