<?php
include_once "ImmutableTriangle.php";

class Triangle extends ImmutableTriangle
{

    public function setBase(int $base): void
    {
        if ($base < 1) {
            throw new Exception("La base d'un triangle doit être > 0, " . $base . " fourni");
        }
        $this->base = $base;
        if (isset($this->height) && isset($this->base)) {
            $this->surface = self::processSurface($this->base, $this->height);
        }

    }

    public function setHeight(int $height): void
    {
        if ($height < 1) {
            throw new Exception("La hauteur d'un triangle doit être > 0, " . $height . " fourni");
        }
        $this->height = $height;
        if ($this->height && $this->base) {
            $this->surface = self::processSurface($this->base, $this->height);
        }
    }



}
